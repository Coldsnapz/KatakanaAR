﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public VideoPlayer vid;


    
    public void Play()
    {
        vid.Play();
        Debug.Log("Play video.");
    }
	
    public void Pause()
    {
        vid.Pause();
    }

    public void Replay()
    {
        vid.Stop();
        vid.Play();
    }

    public void Stop() {
        vid.Stop();
        Debug.Log("Stop video.");
    }
}
