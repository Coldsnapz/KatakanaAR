﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class CardManager : MonoBehaviour
{
    public CardTemplateScript card;

    [Header("ScanMode")] 
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private MeshFilter textMesh;
    
    [Header("Info")]
    [SerializeField] private SpriteRenderer infoImage;
    [SerializeField] private TextMeshProUGUI infoJap;
    [SerializeField] private TextMeshProUGUI infoEng;
    // Start is called before the first frame update
    void Start()
    {
        videoPlayer.clip = card.vid;
        audioSource.clip = card.audio;

        infoImage.sprite = card.infoImage;
        infoJap.text = card.infoJap;
        infoEng.text = card.infoEng;
    }
    
}
