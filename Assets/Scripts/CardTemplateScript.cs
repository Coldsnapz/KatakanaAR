﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(fileName = "NewCard", menuName = "Cards")]
public class CardTemplateScript : ScriptableObject
{
    [Header("ScanMode")] 
    public VideoClip vid;
    public AudioClip audio;
    public Mesh text;
    
    [Header("Info")]
    public Sprite infoImage;
    public string infoJap;
    public string infoEng;

}
